package com.alejosg.productsws;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private EditText productId;
    private EditText productName;
    private EditText productPrice;
    private EditText productCompany;

    private Button btnSave;
    private Button btnEdit;
    private Button btnDelete;
    private Button btnSearch;

    RequestQueue requestQueu;

    private String url = "http://10.5.53.22/PruebaWs";
    private String saveMethod = "Nuevo_Producto.php";
    private String searchMethod = "Buscar_Producto.php";
    private String editMethod = "Editar_Producto.php";
    private String deleteMethod = "Eliminar_Producto.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        productId = findViewById(R.id.txtProductId);
        productName = findViewById(R.id.txtProductName);
        productPrice = findViewById(R.id.txtProductPrice);
        productCompany = findViewById(R.id.txtProductProducer);
        btnSave = findViewById(R.id.btnSave);
        btnSearch = findViewById(R.id.btnSearch);
        btnEdit = findViewById(R.id.btnEdit);
        btnDelete = findViewById(R.id.btnDelete);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullUrl = String.format("%s/%s", url, saveMethod);
                ejecutar(fullUrl);
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = productId.getText().toString();
                String fullUrl = String.format("%s/%s?codigo=%s", url, searchMethod, id);

                searchProduct(fullUrl);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullUrl = String.format("%s/%s", url, editMethod);
                ejecutar(fullUrl);
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullUrl = String.format("%s/%s", url, deleteMethod);
                deleteProduct(fullUrl);
            }
        });
    }

    private void deleteProduct(String fullUrl) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, fullUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Snackbar.make(findViewById(R.id.coordinatorLayout), "Producto se elimino", Snackbar.LENGTH_LONG).show();

                limpiar();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), error.toString(), Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                snackbar.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();

                parameters.put("codigo", productId.getText().toString());

                return parameters;
            }
        };
        requestQueu = Volley.newRequestQueue(this);
        requestQueu.add(stringRequest);
    }

    private void limpiar() {
        productId.setText("");
        productName.setText("");
        productPrice.setText("");
        productCompany.setText("");
    }

    private void searchProduct(String fullUrl) {
        Log.i("search", fullUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(fullUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jsonObject = null;
                //Log.i("search response", jsonObject.toString());
                for (int i = 0; i < response.length(); i++) {
                    try {
                        jsonObject = response.getJSONObject(i);
                        productName.setText(jsonObject.getString("producto"));
                        productPrice.setText(jsonObject.getString("precio"));
                        productCompany.setText(jsonObject.getString("fabricante"));
                    } catch (JSONException e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("serach error", error.toString());
                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), error.toString(), Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                snackbar.show();
            }
        });

        requestQueu = Volley.newRequestQueue(this);
        requestQueu.add(jsonArrayRequest);
        //requestQueu.add(stringRequest);
    }

    private void ejecutar(String fullUrl) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, fullUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Snackbar.make(findViewById(R.id.coordinatorLayout), "Producto registrado", Snackbar.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), error.toString(), Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                snackbar.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("codigo", productId.getText().toString());
                parameters.put("producto", productName.getText().toString());
                parameters.put("precio", productPrice.getText().toString());
                parameters.put("fabricante", productCompany.getText().toString());

                return parameters;
            }
        };

        requestQueu = Volley.newRequestQueue(this);
        requestQueu.add(stringRequest);
    }
}
